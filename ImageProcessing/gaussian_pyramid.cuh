#ifndef __GAUSSIAN_PYRAMID_H__
#define __GAUSSIAN_PYRAMID_H__

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cuda.h>
#include "size_calculator.h"

void buildNextLevelOfPyramid(cudaPitchedPtr src, cudaPitchedPtr dst);

cudaPitchedPtr* calculateGaussianPyramid(cudaPitchedPtr src,int levels);

#endif