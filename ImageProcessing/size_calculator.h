#ifndef __SIZE_CALCULATOR_H__
#define __SIZE_CALCULATOR_H__

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cuda.h>

enum SizeCalculationStrategy
{
	MinimizeAdditions,
	MaximizeThreads	
};

enum SizeCalculationDimension
{
	SqrMatrix,
	RecMatrix
};

void calculateDimensions(int& width, int& height, dim3 gridSize, dim3 blockSize);

void calculateGridSize(dim3& dst, dim3 blockSize, int width, int height);

void calculateOptimalBlockSize(dim3& dst, int width, int height, SizeCalculationStrategy st = MaximizeThreads, SizeCalculationDimension dim = RecMatrix);

void calculateGridAndBlockSizes(dim3& gridSize, dim3& blockSize, int width, int height, SizeCalculationStrategy st = MaximizeThreads, SizeCalculationDimension dim = RecMatrix);

#endif // !_SIZE_CALCULATOR_H_
