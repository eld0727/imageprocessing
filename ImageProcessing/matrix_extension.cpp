#include "matrix_extension.h"

void copyToDst(int** dst, int** src, int width, int height, int x_shift, int y_shift)
{
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			dst[i+y_shift][j+x_shift] = src[i][j];
}

void extendInTail(
	int** dst,
	int** src,
	int width, 
	int height,
	int extendedWidth,
	int extendedHeight,
	MatrixExtansionFilling fill){
		int x_add = extendedWidth - width;
		int y_add = extendedHeight - height;
		int fillWith = 0;
		copyToDst(dst, src, width, height, 0, 0);
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < x_add; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[i][width-1];
				else if(fill == MirrorCopy)
				{
					int a = width - j - 2;
					a = a < 0 ? 0 : a;
					fillWith = src[i][a];
				}
				dst[i][j+width] = fillWith;
			}
		}

		for (int j = 0; j < width; j++)
		{
			for (int i = 0; i < y_add; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height-1][j];
				else if(fill == MirrorCopy)
				{
					int a = height - i - 2;
					a = a < 0 ? 0 : a;
					fillWith = src[a][j];
				}
				dst[i+height][j] = fillWith;
			}
		}

		for (int i = 0; i < y_add; i++)
		{
			int b = height - i - 2;
			b = b < 0 ? 0 : b;
			for (int j = 0; j < x_add; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height - 1][width - 1];
				else if(fill == MirrorCopy)
				{
					int a = width - j - 2;
					a = a < 0 ? 0 : a;
					fillWith = src[b][a];
				}
				dst[i+height][j+width] = fillWith;
			}
		}
}

void extendInHead(
	int** dst,
	int** src,
	int width, 
	int height,
	int extendedWidth,
	int extendedHeight,
	MatrixExtansionFilling fill){
		int x_shift = extendedWidth - width;
		int y_shift = extendedHeight - height;
		int fillWith = 0;
		copyToDst(dst, src, width, height, x_shift, y_shift);

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < x_shift; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[i][0];
				else if(fill == MirrorCopy)
				{
					int a = x_shift - j;
					a = a >= width ? width - 1 : a;
					fillWith = src[i][a];
				}
				dst[i+y_shift][j] = fillWith;
			}
		}

		for (int j = 0; j < width; j++)
		{
			for (int i = 0; i < y_shift; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[0][j];
				else if(fill == MirrorCopy)
				{
					int a = y_shift - i;
					a = a >= height ? height - 1 : a;
					fillWith = src[a][j];
				}
				dst[i][j+x_shift] = fillWith;
			}
		}

		for (int i = 0; i < y_shift; i++)
		{
			int b = y_shift - i;
			b = b >= height ? height - 1 : b;
			for (int j = 0; j < x_shift; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[0][0];
				else if(fill == MirrorCopy)
				{
					int a = x_shift - j;
					a = a >= width ? width - 1 : a;
					fillWith = src[b][a];
				}
				dst[i][j] = fillWith;
			}
		}
}

void extendEvenly(
	int** dst,
	int** src,
	int width, 
	int height,
	int extendedWidth,
	int extendedHeight,
	MatrixExtansionFilling fill){
		int x_add = (extendedWidth - width)/2;
		int y_add = (extendedHeight - height)/2;
		int x_shift = extendedWidth - width - x_add;
		int y_shift = extendedHeight - height - y_add;
		int fillWith = 0;
		copyToDst(dst, src, width, height, x_shift, y_shift);

		//middle
		for (int j = 0; j < width; j++)
		{
			for (int i = 0; i < y_shift; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[0][j];
				else if(fill == MirrorCopy)
				{
					int a = y_shift - i;
					a = a >= height ? height - 1 : a;
					fillWith = src[a][j];
				}
				dst[i][j+x_shift] = fillWith;
			}

			for (int i = 0; i < y_add; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height-1][j];
				else if(fill == MirrorCopy)
				{
					int a = height - i - 2;
					a = a < 0 ? 0 : a;
					fillWith = src[a][j];
				}
				dst[i+y_shift+height][j+x_shift] = fillWith;
			}
		}

		//left
		for (int j = 0; j < x_shift; j++)
		{
			int b = x_shift - j;
			b = b >= width ? width - 1 : b;
			for (int i = 0; i < y_shift; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder) fillWith = src[0][0];
				else if(fill == MirrorCopy)	fillWith = dst[i][b + x_shift];
				dst[i][j] = fillWith;
			}

			for (int i = 0; i < height; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[i][0];
				else if(fill == MirrorCopy)	fillWith = src[i][b];
				dst[i+y_shift][j] = fillWith;
			}

			for (int i = 0; i < y_add; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height - 1][0];
				else if(fill == MirrorCopy)	fillWith = dst[i+height+y_shift][b+x_shift];
				dst[i+y_shift+height][j] = fillWith;
			}
		}

		//right
		for (int j = 0; j < x_add; j++)
		{
			int b = width - j - 2;
			b = b < 0 ? 0 : b;
			for (int i = 0; i < y_shift; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder) fillWith = src[0][width - 1];
				else if(fill == MirrorCopy)	fillWith = dst[i][b + x_shift];
				dst[i][j+width+x_shift] = fillWith;
			}

			for (int i = 0; i < height; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[i][width - 1];
				else if(fill == MirrorCopy)	fillWith = src[i][b];
				dst[i+y_shift][j+width+x_shift] = fillWith;
			}

			for (int i = 0; i < y_add; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height - 1][width - 1];
				else if(fill == MirrorCopy)	fillWith = dst[i+height+y_shift][b+x_shift];
				dst[i+y_shift+height][j+width+x_shift] = fillWith;
			}
		}
}

void extendMatrix(
	int** dst,
	int** src,
	int width, 
	int height,
	int extendedWidth,
	int extendedHeight,
	MatrixExtansionStrategy st,
	MatrixExtansionFilling fill){
		if(st == WriteInTail) extendInTail(dst, src, width, height, extendedWidth, extendedHeight, fill);
		else if(st == WriteInHead) extendInHead(dst, src, width, height, extendedWidth, extendedHeight, fill);
		else if(st == WriteEvenly) extendEvenly(dst, src, width, height, extendedWidth, extendedHeight, fill);
		

		/*int x_shift = 0;
		int y_shift = 0;
		int x_add = 0;
		int y_add = 0;
		int fillWith = 0;

		switch (st)
		{
		case WriteInTail:
			x_add = extendedWidth - width;
			y_add = extendedHeight - height;
			break;
		case WriteInHead:
			x_shift = extendedWidth - width;
			y_shift = extendedHeight - height;
			break;
		case WriteEvenly:
			x_add = (extendedWidth - width) / 2;
			y_add = (extendedHeight - height) / 2;
			x_shift = extendedWidth - width - x_add ;
			y_shift = extendedHeight - height - y_add;
			break;
		default:
			break;
		}
		copyToDst(dst, src, width, height, x_shift, y_shift);

		for (int j = 0; j < width; j++)
		{
			for (int i = 0; i < y_shift; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[0][j];
				else if(fill == MirrorCopy)
				{
					int a = y_shift - i;
					a = a >= height ? height - 1 : a;
					fillWith = src[a][j];
				}
				dst[i][j + x_shift] = fillWith;
			}

			for (int i = 0; i < y_add; i++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height - 1][j];
				else if(fill == MirrorCopy)
				{
					int a = height - y_add + i - 1;
					a = a < 0 ? 0 : a;
					fillWith = src[a][j];
				}
				dst[height + i][j + x_shift] = fillWith;
			}
		}


		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < x_shift; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[i][0];
				else if(fill == MirrorCopy)
				{
					int a = x_shift - j;
					a = a >= width ? width - 1 : a;
					fillWith = src[i][a];
				}
				dst[i + y_shift][j] = fillWith;
			}

			for (int j = 0; j < x_add; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[i][width - 1];
				else if(fill == MirrorCopy)
				{
					int a = width - x_add + j - 1;
					a = a < 0 ? 0 : a;
					fillWith = src[i][a];
				}
				dst[i + y_shift][width + j] = fillWith;
			}
		}

		for (int i = 0; i < y_shift; i++)
		{
			int b = y_shift - i;
			b = b >= height ? height - 1 : b;
			for (int j = 0; j < x_shift; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[0][0];
				else if(fill == MirrorCopy)
				{
					int a = x_shift - j;
					a = a >= width ? width - 1 : a;
					fillWith = src[b][a];
				}
				dst[i][j] = fillWith;
			}
			for (int j = 0; j < x_add; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[0][width - 1];
				else if(fill == MirrorCopy)
				{
					int a = width - x_shift + j - 1;
					a = a < 0 ? 0 : a;
					fillWith = src[b][a];
				}
				dst[i][j+width] = fillWith;
			}
		}

		for (int i = 0; i < y_add; i++)
		{
			int b = height - y_add + i - 1;
			b = b < 0 ? 0 : b;
			for (int j = 0; j < x_shift; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height - 1][0];
				else if(fill == MirrorCopy)
				{
					int a = x_shift - j;
					a = a >= width ? width - 1 : a;
					fillWith = src[b][a];
				}
				dst[i+height][j] = fillWith;
			}
			for (int j = 0; j < x_add; j++)
			{
				if(fill == FillWithZero) fillWith = 0;
				else if(fill == CopyBorder)  fillWith = src[height - 1][width - 1];
				else if(fill == MirrorCopy)
				{
					int a = width - x_shift + j - 1;
					a = a < 0 ? 0 : a;
					fillWith = src[b][a];
				}
				dst[i+height][j+width] = fillWith;
			}
		}*/
}