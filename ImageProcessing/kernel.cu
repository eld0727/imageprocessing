#include "gaussian_pyramid.cuh";

#include <iostream>
#include <conio.h>

#include "test.cuh"
#include "size_calculator.h"
#include "matrix_extension.h"


int additions(dim3 grid, dim3 block, int width, int height)
{
	return grid.x*grid.y*block.x*block.y - width*height;
}

void printMatrix(int** mas, int w, int h)
{
	for (int i = 0; i < h; i++)
	{
		std::cout<<"\t\t";
		for (int j = 0; j < w; j++)
		{
			std::cout<<mas[i][j]<<' ';
		}
		std::cout<<std::endl;
	}
}

int main(){
	int devCount;
	cudaGetDeviceCount(&devCount);
	printf("The Number of CUDA Devices: %d \n\n", devCount);
	for (int i=0; i<devCount; i++)
	{
		printf("Device Information (Device ID = %d) \n \n",i);
		cudaDeviceProp properties;
		cudaGetDeviceProperties(&properties, i);
		printf("The Number of multiprocessors on Device %d \n", properties.multiProcessorCount);
		printf("Size of shared Memory per Block %d Bytes \n", properties.sharedMemPerBlock);
		printf("Maximum Number of Threads per Block %d \n", properties.maxThreadsPerBlock);
		printf("Maximum Grid Size [ %d, %d, %d ] \n", properties.maxGridSize[0], properties.maxGridSize[1], properties.maxGridSize[2]);
		printf("Maximum Threads Dim [ %d, %d, %d ] \n", properties.maxThreadsDim[0], properties.maxThreadsDim[1], properties.maxThreadsDim[2]);
		std::cout<<"Global Memory available on Device "<<properties.totalGlobalMem<<"  Bytes \n";
		printf("Maximum Number of Threads per Multiprocessor %d \n", properties.maxThreadsPerMultiProcessor);
		printf("Warp size %d \n\n\n", properties.warpSize);

	}
	/*int n = 3;

	int** mas = new int*[n];
	for (int i = 0; i < n; i++)
	{
		mas[i] = new int[n];
		for (int j = 0; j < n; j++)
		{
			mas[i][j] = j + 1 + i*n;
		}
	}

	int** nmas;
	int en = n + 4;
	nmas = new int*[en];
	for (int i = 0; i < en; i++)
		nmas[i] = new int[en];
	std::cout<<"\nMatrix extension:\n\n";
	std::cout<<"WriteInHead:\n";
	std::cout<<"\t FillWithZero:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteInHead, FillWithZero);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"\t CopyBorder:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteInHead, CopyBorder);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"\t MirrorCopy:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteInHead, MirrorCopy);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"WriteInTail:\n";
	std::cout<<"\t FillWithZero:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteInTail, FillWithZero);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"\t CopyBorder:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteInTail, CopyBorder);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"\t MirrorCopy:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteInTail, MirrorCopy);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"WriteEvenly:\n";
	std::cout<<"\t FillWithZero:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteEvenly, FillWithZero);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"\t CopyBorder:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteEvenly, CopyBorder);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;
	std::cout<<"\t MirrorCopy:\n";
	extendMatrix(nmas,mas,n,n,en,en,WriteEvenly, MirrorCopy);
	printMatrix(nmas, en, en);
	std::cout<<std::endl;


	int N = 300;
	int M = 705;
	dim3 blockSize;
	dim3 grid;
	printf("\n\nBlock and Grid calculation:\n\n");
	calculateGridAndBlockSizes(grid, blockSize, N, M, MaximizeThreads, SqrMatrix);	
	printf("%dx%d on MaximizeThreads and SqrMatrix:\n\tgrid(%d, %d)\n\tblock(%d, %d)\n\tadditions: %d\n\n", N, M, grid.x, grid.y, blockSize.x, blockSize.y, additions(grid, blockSize, N, M));
	calculateGridAndBlockSizes(grid, blockSize, N, M, MinimizeAdditions, SqrMatrix);	
	printf("%dx%d on MinimizeAdditions and SqrMatrix:\n\tgrid(%d, %d)\n\tblock(%d, %d)\n\tadditions: %d\n\n", N, M, grid.x, grid.y, blockSize.x, blockSize.y, additions(grid, blockSize, N, M));
	calculateGridAndBlockSizes(grid, blockSize, N, M, MaximizeThreads, RecMatrix);	
	printf("%dx%d on MaximizeThreads and RecMatrix:\n\tgrid(%d, %d)\n\tblock(%d, %d)\n\tadditions: %d\n\n", N, M, grid.x, grid.y, blockSize.x, blockSize.y, additions(grid, blockSize, N, M));
	calculateGridAndBlockSizes(grid, blockSize, N, M, MinimizeAdditions, RecMatrix);	
	printf("%dx%d on MinimizeAdditions and RecMatrix:\n\tgrid(%d, %d)\n\tblock(%d, %d)\n\tadditions: %d\n\n", N, M, grid.x, grid.y, blockSize.x, blockSize.y, additions(grid, blockSize, N, M));*/


	/*printf("\n\nPress any key to run a speed test...");
	getch();
	printf("\n\n");



	tests();*/

	printf("\n\nPress any key to quit...");

	getch();
}