#include "size_calculator.h"
#include <cmath>

void calculateDimensions(int& width, int& height, dim3 gridSize, dim3 blockSize)
{
	width = blockSize.x * gridSize.x;
	height = blockSize.y * gridSize.y;
}

void calculateGridSize(dim3& dst, dim3 blockSize, int width, int height)
{
	dst = dim3((width + blockSize.x - 1) / blockSize.x, (height + blockSize.y - 1) / blockSize.y);
}

void calculateOptimalBlockSize(dim3& dst, int width, int height, SizeCalculationStrategy st, SizeCalculationDimension dim)
{
	int device;
	cudaGetDevice(&device);
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, device);
	int sizeX = 0;
	int sizeY = 0;
	int size = 0;
	long additions = 0;
	long currentAddtions = width * height;
	int xExt = 0;
	int yExt = 0;
	int a = 0, b = 0, c = 0;

	switch (st)
	{
	case MinimizeAdditions:
		switch (dim)
		{
		case SqrMatrix:
			size = sqrt(prop.maxThreadsPerBlock);
			for(sizeX = size; sizeX >  size / 2; sizeX--)
			{
				a = width % sizeX;
				xExt = a == 0 ? 0 : sizeX - a;
				a = height % sizeX;
				yExt = a == 0 ? 0 : sizeX - a;
				additions = xExt * height + yExt*(width + xExt);
				if(additions == 0)
				{
					sizeY = sizeX;
					break;
				}
				else if(additions < currentAddtions)
				{
					currentAddtions = additions;
					sizeY = sizeX;
				}
			}
			sizeX = sizeY;
			break;
		case RecMatrix:
			size = prop.maxThreadsPerBlock;
			for(sizeX = size, sizeY = 1; sizeX >= width; sizeX--)
			{
				if(sizeY < height && (sizeY + 1)*sizeX <= size) sizeY++;
				a = width % sizeX;
				xExt = a == 0 ? 0 : sizeX - a;
				a = height % sizeY;
				yExt = a == 0 ? 0 : sizeY - a;
				additions = xExt * height + yExt*(width + xExt);
				if(additions == 0)
				{
					b = sizeX;
					c = sizeY;
					break;
				}
				else if(additions < currentAddtions)
				{
					currentAddtions = additions;
					b = sizeX;
					c = sizeY;
				}
			}
			if(b > 0 || c > 0)
			{
				sizeX = b;
				sizeY = c;
			}
			break;
		default:
			break;
		}
		break;
	case MaximizeThreads:
		switch (dim)
		{
		case SqrMatrix:
			sizeX = sqrt(prop.maxThreadsPerBlock);
			sizeY = sizeX;
			break;
		case RecMatrix:
			size = prop.maxThreadsPerBlock;
			for(sizeX = size, sizeY = 1; sizeX > sizeY; sizeX /= 2, sizeY*=2)
			{
				a = width % sizeX;
				xExt = a == 0 ? 0 : sizeX - a;
				a = height % sizeY;
				yExt = a == 0 ? 0 : sizeY - a;
				additions = xExt * height + yExt*(width + xExt);
				if(additions == 0)
				{
					b = sizeX;
					break;
				}
				else if(additions < currentAddtions)
				{
					currentAddtions = additions;
					b = sizeX;
				}
			}
			if(b > 0) sizeX = b;
			sizeY = size / sizeX;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	dst = dim3(sizeX, sizeY);
}

void calculateGridAndBlockSizes(dim3& gridSize, dim3& blockSize, int width, int height, SizeCalculationStrategy st, SizeCalculationDimension dim)
{
	calculateOptimalBlockSize(blockSize, width, height, st, dim);
	calculateGridSize(gridSize, blockSize, width, height);
}