#include "test.cuh";
#include <iostream>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cuda.h>
#include "size_calculator.h"

__global__ void withOneThread(int* m, int N, int M)
{
	int x = threadIdx.x + blockDim.x * blockIdx.x;
	int y = threadIdx.y + blockDim.y * blockIdx.y;
	int offset = x + y*N;
	for (int i = 0; i < 100; i++)
	{
		m[offset]++;
	}
}

__global__ void maximumThreads(int* m, int N, int M)
{
	int x = threadIdx.x + blockDim.x * blockIdx.x;
	int y = threadIdx.y + blockDim.y * blockIdx.y;
	if(x < N && y < M)
	{
		int offset = x + y*N;
		for (int i = 0; i < 100; i++)
		{
			m[offset]++;
		}
	}
}

void testFun(int N, int M)
{
	int device;
	cudaGetDevice(&device);
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, device);
	std::cout<<"Start of  "<<N<<'x'<<M<<" test:\n";
	std::cout<<"\tLinear array:\n";
	int* mas = new int[N*M];
	int* dmas;
	for (int i = 0; i < N*M; i++)
	{
		mas[i] = 0;
	}

	cudaMalloc(&dmas, N*M*sizeof(int));
	cudaMemcpy(dmas, mas,N*M*sizeof(int), cudaMemcpyHostToDevice);

	cudaEvent_t start;
	cudaEvent_t stop;
	float elapsedTime;

	//One thread proccessing
	cudaEventCreate(&start);
	cudaEventRecord(start,0);

	withOneThread<<<dim3(N,M), dim3(1,1)>>>(dmas, N, M);

	cudaEventCreate(&stop);
	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);

	std::cout<<"\t\tTime with single thread "<<elapsedTime<<" ms\n";
	//� ���������� ��������
	dim3 block;
	dim3 grid;
	calculateGridAndBlockSizes(grid, block, N, M, MaximizeThreads, SqrMatrix);

	cudaEventRecord(start,0);

	maximumThreads<<<grid, block>>>(dmas, N, M);

	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);

	std::cout<<"\t\tTime with maximum threads and sqr matrix "<<elapsedTime<<" ms\n";
	//����������� ����� �����
	calculateGridAndBlockSizes(grid, block, N, M, MaximizeThreads, SqrMatrix);

	cudaEventRecord(start,0);

	maximumThreads<<<grid, block>>>(dmas, N, M);

	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);

	std::cout<<"\t\tTime with maximum threads and minimum lines "<<elapsedTime<<" ms\n";

	//����������� �������
	if(N > prop.maxThreadsPerBlock)
		block = dim3(1, prop.maxThreadsPerBlock);
	else
		block = dim3(prop.maxThreadsPerBlock / N, N);
	calculateGridSize(grid, block, N, M);

	cudaEventRecord(start,0);

	maximumThreads<<<grid, block>>>(dmas, N, M);

	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);

	std::cout<<"\t\tTime with maximum threads and minimum columns "<<elapsedTime<<" ms\n";

	//����������� ������ ���������
	calculateGridAndBlockSizes(grid, block, N, M, MinimizeAdditions, RecMatrix);

	cudaEventRecord(start,0);

	maximumThreads<<<grid, block>>>(dmas, N, M);

	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);

	std::cout<<"\t\tTime with minimum additional elements "<<elapsedTime<<" ms\n";

	cudaFree(dmas);
	delete mas;
	std::cout<<"End of  "<<N<<'x'<<M<<" test\n\n";
}

void tests()
{
	std::cout<<"Speed test:\n\n";
	for(int N = 31; N <= 2048; N*=2)
	{
		testFun(N, N/2 + 30);
	}

}
