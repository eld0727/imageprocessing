#ifndef __MATRIX_EXTENSION_H__
#define __MATRIX_EXTENSION_H__

enum MatrixExtansionStrategy
{
	WriteInTail,
	WriteInHead,
	WriteEvenly
};

enum MatrixExtansionFilling
{
	FillWithZero,
	CopyBorder,
	MirrorCopy
};

void extendMatrix(
	int** dst,
	int** src,
	int width, 
	int height,
	int extendedWidth,
	int extendedHeight,
	MatrixExtansionStrategy st = WriteInTail,
	MatrixExtansionFilling fill = FillWithZero);

#endif //__MATRIX_EXTENSION_H__