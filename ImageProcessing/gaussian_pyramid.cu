#include "gaussian_pyramid.cuh"

__global__ void buildHorizontalBorders(cudaPitchedPtr src, cudaPitchedPtr dst)
{
	size_t offset = threadIdx.x + blockIdx.x * blockDim.x + 1;
	if(offset < dst.xsize - 1)
	{
		size_t srcOffset = offset * 2 - 1;
		int sum = 0;
		char* row1 = (char*)src.ptr;
		char* row2 = row1;
		char* row3 = row2 + src.pitch;
		char* row4 = row3 + src.pitch;
		sum += row1[srcOffset] + row1[srcOffset + 1] + row1[srcOffset + 2] + row1[srcOffset + 3];
		sum += row2[srcOffset] + row2[srcOffset + 1] + row2[srcOffset + 2] + row2[srcOffset + 3];
		sum += row3[srcOffset] + row3[srcOffset + 1] + row3[srcOffset + 2] + row3[srcOffset + 3];
		sum += row4[srcOffset] + row4[srcOffset + 1] + row4[srcOffset + 2] + row4[srcOffset + 3];
			
		((char*)(dst.ptr))[offset] = sum / 16;
		size_t srcYOffset = (dst.ysize - 1) * 2 - 1;
		row1 = ((char*)src.ptr) + src.pitch * srcYOffset;
		row2 = row1 + src.pitch;
		row3 = row2 + src.pitch;
		row4 = ((char*)src.ptr) + src.pitch * (src.ysize - 1);
		sum = 0;
		sum += row1[srcOffset] + row1[srcOffset + 1] + row1[srcOffset + 2] + row1[srcOffset + 3];
		sum += row2[srcOffset] + row2[srcOffset + 1] + row2[srcOffset + 2] + row2[srcOffset + 3];
		sum += row3[srcOffset] + row3[srcOffset + 1] + row3[srcOffset + 2] + row3[srcOffset + 3];
		sum += row4[srcOffset] + row4[srcOffset + 1] + row4[srcOffset + 2] + row4[srcOffset + 3];
		(((char*)dst.ptr) + dst.pitch * (dst.ysize - 1))[offset] = sum / 16;		
	}
}

__global__ void buildVerticalBorders(cudaPitchedPtr src, cudaPitchedPtr dst)
{
	size_t offset = threadIdx.x + blockIdx.x * blockDim.x + 1;
	if(offset < dst.xsize - 1)
	{
		size_t srcOffset = offset * 2 - 1;
		int sum = 0;
		char* row1 = ((char*)src.ptr) + src.pitch * srcOffset;
		char* row2 = row1 + src.pitch;
		char* row3 = row2 + src.pitch;
		char* row4 = row3 + src.pitch;
		sum += row1[0] + row1[0] + row1[1] + row1[2];
		sum += row2[0] + row2[0] + row2[1] + row2[2];
		sum += row3[0] + row3[0] + row3[1] + row3[2];
		sum += row4[0] + row4[0] + row4[1] + row4[2];
			
		(((char*)dst.ptr) + offset * dst.pitch)[0] = sum / 16;
		size_t srcXOffset = (dst.xsize - 1) * 2 - 1;
		size_t srcRightBorderOffset = src.xsize - 1;
		sum = 0;
		sum += row1[srcXOffset] + row1[srcXOffset + 1] + row1[srcXOffset + 2] + row1[srcRightBorderOffset];
		sum += row2[srcXOffset] + row2[srcXOffset + 1] + row2[srcXOffset + 2] + row2[srcRightBorderOffset];
		sum += row3[srcXOffset] + row3[srcXOffset + 1] + row3[srcXOffset + 2] + row3[srcRightBorderOffset];
		sum += row4[srcXOffset] + row4[srcXOffset + 1] + row4[srcXOffset + 2] + row4[srcRightBorderOffset];
		(((char*)dst.ptr) + offset * dst.pitch)[dst.xsize - 1] = sum / 16;
	}
}

__global__ void buildBody(cudaPitchedPtr src, cudaPitchedPtr dst)
{
	size_t x = threadIdx.x + blockIdx.x * blockDim.x + 1;
	size_t y = threadIdx.y + blockIdx.y * blockDim.y + 1;
	if(x < dst.xsize - 1 && y < dst.ysize - 1)
	{
		size_t srcX = x + x;
		int sum = 0;
		char* row1 = ((char*)src.ptr) + src.pitch * y * 2;
		char* row2 = row1 + src.pitch;
		char* row3 = row2 + src.pitch;
		char* row4 = row3 + src.pitch;
		sum += row1[srcX + 0] + row1[srcX + 1] + row1[srcX + 2] + row1[srcX + 3];
		sum += row2[srcX + 0] + row2[srcX + 1] + row2[srcX + 2] + row2[srcX + 3];
		sum += row3[srcX + 0] + row3[srcX + 1] + row3[srcX + 2] + row3[srcX + 3];
		sum += row4[srcX + 0] + row4[srcX + 1] + row4[srcX + 2] + row4[srcX + 3];
		(((char*)dst.ptr) + y * dst.pitch)[x] = sum / 16;
	}
}

//single thread
__global__ void buildCorners(cudaPitchedPtr src, cudaPitchedPtr dst)
{
	//top left
	int sum = 0;
	char* row1 = (char*)src.ptr;
	char* row2 = row1;
	char* row3 = row2 + src.pitch;
	char* row4 = row3 + src.pitch;
	sum += row1[0] + row1[0] + row1[1] + row1[2];
	sum += row2[0] + row2[0] + row2[1] + row2[2];
	sum += row3[0] + row3[0] + row3[1] + row3[2];
	sum += row4[0] + row4[0] + row4[1] + row4[2];		
	((char*)(dst.ptr))[0] = sum / 16;

	// top right
	size_t srcXOffset = (dst.xsize - 1) * 2 - 1;
	size_t srcRightBorderOffset = src.xsize - 1;
	sum = 0;
	sum += row1[srcXOffset] + row1[srcXOffset + 1] + row1[srcXOffset + 2] + row1[srcRightBorderOffset];
	sum += row2[srcXOffset] + row2[srcXOffset + 1] + row2[srcXOffset + 2] + row2[srcRightBorderOffset];
	sum += row3[srcXOffset] + row3[srcXOffset + 1] + row3[srcXOffset + 2] + row3[srcRightBorderOffset];
	sum += row4[srcXOffset] + row4[srcXOffset + 1] + row4[srcXOffset + 2] + row4[srcRightBorderOffset];
	((char*)(dst.ptr))[dst.xsize - 1] = sum / 16;

	// bottom left
	size_t srcYOffset = (dst.ysize - 1) * 2 - 1;
	row1 = ((char*)src.ptr) + src.pitch * srcYOffset;
	row2 = row1 + src.pitch;
	row3 = row2 + src.pitch;
	row4 = ((char*)src.ptr) + src.pitch * (src.ysize - 1);
	sum = 0;
	sum += row1[0] + row1[0] + row1[1] + row1[2];
	sum += row2[0] + row2[0] + row2[1] + row2[2];
	sum += row3[0] + row3[0] + row3[1] + row3[2];
	sum += row4[0] + row4[0] + row4[1] + row4[2];		
	((char*)dst.ptr + dst.pitch * (dst.ysize - 1))[0] = sum / 16;

	// bottom right
	sum = 0;
	sum += row1[srcXOffset] + row1[srcXOffset + 1] + row1[srcXOffset + 2] + row1[srcRightBorderOffset];
	sum += row2[srcXOffset] + row2[srcXOffset + 1] + row2[srcXOffset + 2] + row2[srcRightBorderOffset];
	sum += row3[srcXOffset] + row3[srcXOffset + 1] + row3[srcXOffset + 2] + row3[srcRightBorderOffset];
	sum += row4[srcXOffset] + row4[srcXOffset + 1] + row4[srcXOffset + 2] + row4[srcRightBorderOffset];
	((char*)dst.ptr + dst.pitch * (dst.ysize - 1))[dst.xsize - 1] = sum / 16;
}

void buildNextLevelOfPyramid(cudaPitchedPtr src, cudaPitchedPtr dst)
{
	dim3 grid;
	dim3 block;
	calculateGridAndBlockSizes(grid, block, dst.xsize - 2, 1, SizeCalculationStrategy::MinimizeAdditions, SizeCalculationDimension::RecMatrix);
	buildHorizontalBorders<<<grid, block>>>(src, dst);
	calculateGridAndBlockSizes(grid, block, dst.ysize - 2, 1, SizeCalculationStrategy::MinimizeAdditions, SizeCalculationDimension::RecMatrix);
	buildVerticalBorders<<<grid, block>>>(src, dst);
	calculateGridAndBlockSizes(grid, block, dst.xsize - 2, dst.ysize - 2, SizeCalculationStrategy::MaximizeThreads, SizeCalculationDimension::RecMatrix);
	buildBody<<<grid, block>>>(src, dst);
	buildCorners<<<1,1>>>(src, dst);

}

cudaPitchedPtr* calculateGaussianPyramid(cudaPitchedPtr src,int levels)
{
	cudaPitchedPtr* pyramids = new cudaPitchedPtr[levels];
	pyramids[0] = src;
	void* ptr;
	size_t p;
	size_t w = src.xsize;
	size_t h = src.ysize;
	cudaMallocPitch(&ptr, &p, w, h);
	cudaMemcpy2D(ptr, p, src.ptr, src.pitch, w, h, cudaMemcpyDeviceToDevice);
	pyramids[0] = make_cudaPitchedPtr(ptr, p, w, h);
	for (int i = 1; i < levels; i++)
	{
		w = pyramids[i - 1].xsize / 2;
		h = pyramids[i - 1].ysize / 2;
		cudaMallocPitch(&ptr, &p, w, h);
		pyramids[i] = make_cudaPitchedPtr(ptr, p, w, h);
		buildNextLevelOfPyramid(pyramids[i - 1], pyramids[i]);
	}
	return pyramids;
}